package logger

import (
	"context"
	"github.com/sirupsen/logrus"
)

func InitLogger(ctx context.Context, logger *logrus.Entry) context.Context {
	return context.WithValue(ctx, LoggerKey, logger)
}

func InitLoggerWithTrace(ctx context.Context, traceId string) context.Context {
	logger := GetLogger(ctx).WithField(TraceKey, traceId)
	return InitLogger(ctx, logger)
}

func GetLogger(ctx context.Context) *logrus.Entry {
	if logger, ok := ctx.Value(LoggerKey).(*logrus.Entry); ok {
		return logger
	}
	return logrus.NewEntry(logrus.StandardLogger())
}
